// console.log('Happy Monday?');

// Fetch Keyword
    // Syntax:
        // fetch('url',{options});
        // in options - method, body and headers

    // GET post data

    fetch('https://jsonplaceholder.typicode.com/posts')
    .then (response =>  { 
        // console.log(response.json()) 
        return response.json()
    }).then(result =>{
        console.log(result);
        showPosts(result)
    })

// Show post
    // create a function that will let us show the post from our API


const showPosts = (posts) => {
    let entries = ``;

    posts.forEach((post) => {
        entries += `
        <div id = "post-${post.id}" ->
            <h3 id = "post-title-${post.id}">${post.title}</h3>
            <p id = "post-body-${post.id}">${post.body}</p>
            <button  onclick="editPost(${post.id})">Edit</button>
            <button onclick="deletePost(${post.id})">Delete</button>

        </div>
        `
    })

    document.querySelector("#div-post-entries").innerHTML = entries
}

// POST data on our API
// target the form for adding post
document.querySelector("#form-add-post").addEventListener("submit", (event) => {

    // change the autoreaload of the submit method 
    event.preventDefault();
    // post method
    fetch('https://jsonplaceholder.typicode.com/posts',{
        method: 'POST',
        body: JSON.stringify({
            title:document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId: 1
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(response => response.json())
    .then (result => {
        console.log(result)

        
        document.querySelector("#txt-title").value = null;
        document.querySelector("#txt-body").value = null;

        alert("Post is successfully added!");


    })
})

// Edit post
// (id) para pag niclick ung edit button ng every post, makukuha nya ung id
const editPost = (id) => {
    console.log(id);
    // this one naman para pag click ng edit button ng post, makukuha ung info ng post like the title and body
    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-body-${id}`).innerHTML

    console.log(title);
    console.log(body);

    // this one ipapasa dun sa edit form ung nakuhang info from clicking edit button of post
    document.querySelector("#txt-edit-title").value = title;
    document.querySelector("#txt-edit-body").value = body;
    document.querySelector("#txt-edit-id").value = id;

    // this one para ma enable ung update button na nakadisable sa html file pag cla click sa edit button ng post
    document.querySelector('#btn-submit-update').removeAttribute('disabled')

}

document.querySelector("#form-edit-post").addEventListener("submit",(event) =>{
    event.preventDefault();

    let id = document.querySelector('#txt-edit-id').value
    fetch (`https://jsonplaceholder.typicode.com/posts/${id}`,{
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            title:document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            id: id,
            userId: 1
        })
    })
    .then(response => response.json())
    .then(result =>{
        console.log(result);

        alert("Post is successfully updated!");

        document.querySelector('#txt-edit-title').value=null
        document.querySelector('#txt-edit-body').value=null
        document.querySelector('#txt-edit-id').value=null

        document.querySelector('#btn-submit-update').setAttribute('disabled', true)
    })

    
})


const deletePost = (id) => {
    console.log(id);
    fetch (`https://jsonplaceholder.typicode.com/posts`)
    .then (response =>  { 
        return response.json()
    }).then(result =>{
        console.log(result);
        const postIndex = result.findIndex(post => {
            return post.id == id
        })
        result.splice(postIndex, 1)
        document.querySelector(`#post-${id}`).remove()
        // showPosts(result)
    })
    


    
}